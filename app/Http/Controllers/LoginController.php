<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Foundation\Auth\AuthenticatesUsers;


class LoginController extends Controller
{
	public function view()
    {
        return view('login');
    }

    use AuthenticatesUsers;

    public function authenticate(Request $request)
    {
        // return $request;
        $validator = $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:5'
        ]);


        $email = $request->email;

       // $password = Hash::make($request->password);
        $password = $request->password;
        $users = User::where('email', $email)->first();
        //return $users;
        $id = $users->id;
    
       //$credentials = $request->only('email', 'password');
       if (Auth::attempt(array('email' => $email, 'password' => $password)))
        {
                   // to admin dashboard
        if($users->isAdmin()) {
            return redirect(route('employees'));
        }

        // to user dashboard
        else if($users->isUser()) {
            return redirect('notes');
        }

        abort(404);  
        }
        // else{
        //     Log::error('Oppes! You have entered invalid credentials');
        //     return redirect('userlogin')->with('error', 'Oppes! You have entered invalid credentials');
        // }
    }
    
}
