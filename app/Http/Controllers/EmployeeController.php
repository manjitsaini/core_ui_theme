<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\employee;

class EmployeeController extends Controller
{
    public function employeeList(Request $request){

    	$employees = employee::all();
    	//return $employees;
    	return view('dashboard.employee.employee1', compact('employees'));
    }

    public function create(Request $request){

    	// $employees = employee::all();
    	//return $employees;
    	return view('dashboard.employee.create');
    }

    public function store(Request $request)
    {

        $validatedData = $request->validate([
            'name'             => 'required|min:1|max:64',
            'email'           => 'required',
            'address'         => 'required',
            'phone_number'         => 'required|max:10'
        ]);
        //dd($validatedData);
        $employee = employee::all();
        $new_employee = new employee();
        $new_employee->name     = $request->input('name');
        $new_employee->email   = $request->input('email');
        $new_employee->address = $request->input('address');
        $new_employee->phone_number = $request->input('phone_number');
        $new_employee->save();
        $request->session()->flash('message', 'Successfully created employee');
        return redirect()->route('employees');
    }

    public function show(Request $request)
    {
    	$id = $request->id;
        $employee_view = employee::where('id',$id)->first();
        // $id = $employee_view->id;
        return view('dashboard.employee.show', compact('employee_view'));
    }

    public function edit(Request $request)
    {
    	$id = $request->id;
        $employee_edit = employee::where('id',$id)->first();
        // return $employee_edit;
        return view('dashboard.employee.edit', compact('employee_edit'));
    }

    public function update(Request $request)
    {
    	// dd($request);
        //var_dump('bazinga');
        //die();
        $validatedData = $request->validate([
            'name'             => 'required|min:1|max:64',
            'email'           => 'required',
            'address'         => 'required',
            'phone_number'         => 'required|max:10'
        ]);
        //dd($validatedData);
        $id = $request->edit_user_id;
        // return $id;
        $employee_details = employee::where('id',$id)->first();
        // return $employee_details;
        $employee_details->name     = $request->input('name');
        $employee_details->email   = $request->input('email');
        $employee_details->address = $request->input('address');
        $employee_details->phone_number = $request->input('phone_number');
        $employee_details->save();
        $request->session()->flash('message', 'Successfully edited employee');
        return redirect()->route('employees');
    }


    public function destroy(Request $request){
        $id = $request->id;
        // return $id;
        $deleteData = employee::where('id',$id)->first();
        $deleteData->delete();
        return redirect('/employees')->with('success', 'Record deleted successfully');       
    }
}
