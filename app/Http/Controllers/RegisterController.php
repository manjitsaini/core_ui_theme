<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;

class RegisterController extends Controller
{
    public function create() {
        return view('registration');
    }

    public function store(Request $request){
    	//return $request;
    	$validator = $request->validate([
            'name' => 'required',
            'email' => 'required|email|max:50|unique:users',
            'password' => 'required|min:5'
        ]);
        $emailExists = User::where('email', $request->email)->first();

        if(!empty($emailExists->email)){
        	return ['code' => 402, 'status' => 'error', 'data' => $emailExists, 'message' => 'Email already exist'];
        }

        	$user_obj = new User;
            $user_obj->name = $request->name;
            $user_obj->email = $request->email;
            $user_obj->password=  Hash::make($request->password);
            $user_obj->c_password=  $request->password;
            $user_obj->userstype = 'admin';
            $user_obj->role = $request->role;
            $user_obj->save();

        Log::info('Create new user successfully');
        return redirect('/userlogin')->with('success', 'Create new user successfully.');    
    }

}
