@extends('dashboard.base')

@section('content')

        <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card">
                    <div class="card-header">
                      <i class="fa fa-align-justify"></i>{{ __('Employees') }}</div>
                    <div class="card-body">
                        <div class="row"> 
                          <a href="{{ route('emplyeCreate') }}" class="btn btn-primary m-2">{{ __('Add Employee') }}</a>
                        </div>
                        <br>
                        <table class="table table-responsive-sm table-striped">
                        <thead>
                          <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Address</th>
                            <th>Phone Number</th>
                            <th>Applies to date</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($employees as $employee)
                            <tr>
                              <td><strong>{{ $employee->name }}</strong></td>
                              <td><strong>{{ $employee->email }}</strong></td>
                              <td>{{ $employee->address }}</td>
                              <td>{{ $employee->phone_number }}</td>
                              <td>{{ $employee->created_at }}</td>
                              <td>
                                <a href="{{route('employees-profile',['id' => $employee->id])}}" class="btn btn-block btn-primary">View</a>
                              </td>
                              <td>
                                <a href="{{route('employees-profile-edit',['id' => $employee->id])}}" class="btn btn-block btn-primary">Edit</a>
                              </td>
                              <td>
                                <form action="{{ route('delete-employee')}}" method="POST">
                                  <input type="hidden" name="id" value="{{$employee->id}}">
                                    @method('DELETE')
                                    @csrf
                                    <button class="btn btn-block btn-danger">Delete</button>
                                </form>
                              </td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>

@endsection


@section('javascript')

@endsection

