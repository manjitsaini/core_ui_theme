@extends('dashboard.base')

@section('content')

        <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-sm-12 col-md-10 col-lg-8 col-xl-6">
                <div class="card">
                    <div class="card-header">
                    <div class="card-body">
                        <form method="POST" action="{{ route('edit-employees-details') }}">
                            @csrf
                            <input type="hidden" name="edit_user_id" value="{{$employee_edit->id}}">
                            <div class="form-group row">
                                <div class="col">
                                    <label>Name</label>
                                    <input class="form-control" type="text" placeholder="{{ __('name') }}" name="name" value="{{ $employee_edit->name }}" required autofocus>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col">
                                    <label>Email</label>
                                    <input class="form-control" type="text" placeholder="{{ __('email') }}" name="email" value="{{ $employee_edit->email }}" required autofocus>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col">
                                    <label>Address</label>
                                    <textarea class="form-control" id="textarea-input" name="address" rows="9" placeholder="{{ __('address..') }}" required>{{ $employee_edit->address }}</textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col">
                                    <label>Phone Number</label>
                                    <input class="form-control" type="text" placeholder="{{ __('phone number') }}" name="phone_number" value="{{ $employee_edit->phone_number }}" required autofocus>
                                </div>
                            </div>
 
                            <button class="btn btn-block btn-success" type="submit">{{ __('Save') }}</button>
                            <a href="{{ route('employees') }}" class="btn btn-block btn-primary">{{ __('Return') }}</a> 
                        </form>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>

@endsection

@section('javascript')

@endsection