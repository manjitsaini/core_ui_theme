@extends('dashboard.base')

@section('content')

        <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-sm-12 col-md-12 col-lg-8 col-xl-8">
                <div class="card">
                    <div class="card-header">
                      <i class="fa fa-align-justify"></i>{{ __('Employees') }}</div>
                    <div class="card-body">
                      <div class="row"> 
                          <a href="{{ route('emplyeCreate') }}" class="btn btn-primary m-2">{{ __('Add Employee') }}</a>
                        </div>
                        <table class="table table-responsive-sm table-striped">
                        <thead>
                          <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Address</th>
                            <th>Phone Number</th>
                            <th>Applies_to_date</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($employees as $employee)
                            <tr>
                              <td>{{ $employee->name }}</td>
                              <td>{{ $employee->email }}</td>
                              <td>{{ $employee->address }}</td>
                              <td>{{ $employee->phone_number }}</td>
                              <td>{{ $employee->created_at }}</td>
                              <td>
                                <a href="" class="btn btn-block btn-primary">View</a>
                              </td>
                              <td>
                                <a href="" class="btn btn-block btn-primary">Edit</a>
                              </td>
                              <td>
                              <form style="display: inline-block;" action="" method="POST">
                                      <input type="hidden" name="id" value="{{$employee->id}}">
                                                    @csrf
                                                 @method('DELETE')
      
                                      <button type="submit" class="btn btn-block btn-danger" title="Delete Profile">Delete</button>
                                </form>
                              </td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>

@endsection


@section('javascript')

@endsection
