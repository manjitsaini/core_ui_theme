@extends('dashboard.base')

@section('content')

        <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-sm-12 col-md-10 col-lg-8 col-xl-6">
                <div class="card">
                    <div class="card-header">
                      <i class="fa fa-align-justify"></i> {{ __('Create Employee') }}</div>
                    <div class="card-body">
                        <form method="POST" action="{{route('employeestore')}}">
                            @csrf
                            <div class="form-group row">
                                <label>Name</label>
                                <input class="form-control" type="text" placeholder="{{ __('Name') }}" name="name" required autofocus>
                            </div>

                            <div class="form-group row">
                                <label>Email</label>
                                <input class="form-control" type="text" placeholder="{{ __('Email') }}" name="email" required autofocus>
                            </div>

                            <div class="form-group row">
                                <label>Address</label>
                                <textarea class="form-control" id="textarea-input" name="address" rows="9" placeholder="{{ __('Address..') }}" required></textarea>
                            </div>

                            <div class="form-group row">
                                <label>Phone Number</label>
                                <input class="form-control" type="text" placeholder="{{ __('Phone Number') }}" name="phone_number" required autofocus>
                            </div>
 
                            <button class="btn btn-block btn-success" type="submit">{{ __('Add') }}</button>
                            <a href="{{ route('employees') }}" class="btn btn-block btn-primary">{{ __('Return') }}</a> 
                        </form>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>

@endsection

@section('javascript')

@endsection