@extends('dashboard.base')

@section('content')

        <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-sm-12 col-md-10 col-lg-8 col-xl-6">
                <div class="card">
                    <div class="card-header">
                    <div class="card-body">
                        <form method="POST" action="{{ route('logout') }}"> @csrf<button class="btn btn-primary">{{ __('Logout') }}</button></form> 
                        <br>
                        <h4>Name:</h4>
                        <p> {{ $employee_view->name }}</p>
                        <h4>Email:</h4>                                             
                        <p> {{ $employee_view->email }}</p>
                        <h4>Address:</h4> 
                        <p>{{ $employee_view->address }}</p>
                        <h4>Phone Number:</h4> 
                        <p>{{ $employee_view->phone_number }}</p>
                        <h4>Applies to date:</h4> 
                        <p>{{ $employee_view->created_at }}</p>
                        <a href="{{ route('employees') }}" class="btn btn-block btn-primary">{{ __('Return') }}</a> 
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>

@endsection


@section('javascript')

@endsection