@extends('dashboard.base')

@section('content')

        <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-sm-12 col-md-10 col-lg-8 col-xl-6">
                <div class="card">
                    <div class="card-header">
                      <i class="fa fa-align-justify"></i> {{ __('Login Form') }}</div>
                    <div class="card-body">
                        @if(Session::has('success'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                {{Session::get('success')}}
                            </div>
                        @elseif(Session::has('failed'))
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                {{Session::get('failed')}}
                            </div>
                        @endif
                        <form method="POST" action="/logged_in">
                            @csrf

                            <div class="form-group row">
                                <label for="email"> Email <span class="text-danger"> * </span> </label>
                                <input class="form-control" type="text" placeholder="{{ __('Email') }}" name="email" required autofocus>
                                {!!$errors->first("email", "<span class='text-danger'>:message</span>")!!}
                            </div>


                            <div class="form-group row">
                                <label for="password"> Password <span class="text-danger"> * </span></label>
                                <input class="form-control" type="password" placeholder="{{ __('Password') }}" name="password" required autofocus>
                                {!!$errors->first("password", "<span class='text-danger'>:message</span>")!!}
                            </div>
                         
                            <button class="btn btn-block btn-success" type="submit">{{ __('Login') }}</button>
             
                        </form>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>

@endsection

@section('javascript')

@endsection